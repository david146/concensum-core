Building Bitcoin
================

See doc/build-*.md for instructions on building the various
elements of the Bitcoin Core reference implementation of Bitcoin.

## Building with docker

First create the builder image (only needs to be done the first time).

```
docker build -t copytrack-builder .
```

Every time you want to build:

```
docker run -v `pwd`:/opt/copytrack copytrack-builder
```
