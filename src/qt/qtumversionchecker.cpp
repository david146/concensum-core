#include "qtumversionchecker.h"
#include "../clientversion.h"

#include <boost/foreach.hpp>

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QEventLoop>

#include <QJsonDocument>
#include <QJsonObject>

QtumVersionChecker::QtumVersionChecker(QObject *parent) : QObject(parent)
{
    currentVersion = Version(CLIENT_VERSION_MAJOR, CLIENT_VERSION_MINOR, CLIENT_VERSION_REVISION);
}

QtumVersionChecker::~QtumVersionChecker()
{

}

Version QtumVersionChecker::getCurrentVersion()
{
    return currentVersion;
}

Version QtumVersionChecker::getLatestVersion()
{
    QNetworkAccessManager manager;
    QNetworkReply *response = manager.get(QNetworkRequest(QUrl(CONCENSUM_RELEASE_INFO)));

    QEventLoop event;
    connect(response, SIGNAL(finished()), &event, SLOT(quit()));
    event.exec();

    QByteArray jsonByteArr = response->readAll();
    QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonByteArr);
    QVariantMap json = jsonDoc.object().toVariantMap();

    QString versionString = json[CONCENSUM_RELEASE_INFO_LATEST_VERSION].toString();
    QString releaseUrl = json[versionString].toMap()[CONCENSUM_RELEASE_INFO_RELEASE_URL].toString();

    return Version(versionString, releaseUrl);
}

bool QtumVersionChecker::isVersionOutdated()
{
    Version latestVersion = getLatestVersion();

    if (latestVersion > currentVersion)
    {
        return true;
    }
    else
    {
        return false;
    }
}

QString QtumVersionChecker::getDownloadPage()
{
    return CONCENSUM_DOWNLOAD_PAGE;
}
