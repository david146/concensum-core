
Debian
====================
This directory contains files used to package concensum/concensum-qt
for Debian-based Linux systems. If you compile concensum/concensum-qt yourself, there are some useful files here.

## concensum: URI support ##


concensum-qt.desktop  (Gnome / Open Desktop)
To install:

	sudo desktop-file-install concensum-qt.desktop
	sudo update-desktop-database

If you build yourself, you will either need to modify the paths in
the .desktop file or copy or symlink your concensum-qt binary to `/usr/bin`
and the `../../share/pixmaps/bitcoin128.png` to `/usr/share/pixmaps`

concensum-qt.protocol (KDE)

